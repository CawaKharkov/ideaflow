<?php

namespace IdeaFlow\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserSignupType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', 'email', ['required' => true, 'attr' => ['class' => 'form-control input-lg',
            ]])
            ->add('username', 'text', ['required' => true, 'attr' => ['class' => 'form-control input-lg',
            ]])
            ->add('password', 'password', ['required' => true, 'label' => 'Password',
                'attr' => ['class' => 'form-control', 'id' => 'form_password', 'style' => 'width:48%',
                ]])
            ->add('password', 'repeated', array(
                'type' => 'password',
                'invalid_message' => 'The password fields must match.',
                'options' => array('attr' => array('class' => 'form-control', 'style' => 'width:48%')),
                'required' => true,
                'first_options' => array('label' => 'Password'),
                'second_options' => array('label' => 'Repeat Password'),
            ))
            ->add('signup', 'submit', ['attr' => ['class' => 'btn btn-default']]);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'IdeaFlow\UserBundle\Entity\User',
            'validation_groups' => ['signup'],
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'user_signup';
    }

}

