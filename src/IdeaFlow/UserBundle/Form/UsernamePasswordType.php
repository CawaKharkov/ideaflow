<?php

namespace IdeaFlow\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UsernamePasswordType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', 'text', ['required' => true, 'label' =>false,
                'attr' => ['class' => '']]);

    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'username_password_type';
    }

}

