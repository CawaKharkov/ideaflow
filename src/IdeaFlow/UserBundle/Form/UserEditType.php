<?php

namespace IdeaFlow\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use VswSystem\SecurityBundle\Entity\User;

class UserEditType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('email', 'email', ['required' => true, 'attr' => ['class' => 'form-control',
        'placeholder' => 'Email']])
        ->add('username', 'text', ['required' => true, 'attr' => ['class' => 'form-control',
        'placeholder' => 'Username']])
        ->add('role', 'choice', array(
        'choices' => User::getRoleList(),
        'required' => false,
        ))
        ->add('save', 'submit', ['attr' => ['class' => 'btn btn-success']]);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'VswSystem\SecurityBundle\Entity\User',
            'validation_groups' => ['edit_admin'],
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'admin_user_edit';
    }

}

