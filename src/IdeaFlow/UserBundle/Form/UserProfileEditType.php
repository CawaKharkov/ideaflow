<?php

namespace VswSystem\SecurityBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use VswSystem\SecurityBundle\Entity\UserProfile;

class UserProfileEditType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', 'text', ['required' => false,
                'attr' => ['class' => 'form-control input-lg', 'placeholder' => 'Name']])
            ->add('surname', 'text', ['required' => false,
                'attr' => ['class' => 'form-control input-lg', 'placeholder' => 'Surname']])
            ->add('userPhone', 'number', ['required' => false,
                'attr' => ['class' => 'form-control input-lg', 'placeholder' => 'Phone number']])
            ->add('about', 'textarea', ['required' => false,
                'attr' => ['class' => 'form-control input-lg', 'placeholder' => 'About','rows'=>7]])
            ->add('bloodGroup', 'choice', ['choices' => UserProfile::getBloodGroups(),
                'required' => false,'expanded' =>false,'multiple'=>false,
                    'attr' => ['class'=>'selector skip_these']])
            ->add('bloodRh', 'choice', ['choices' => UserProfile::getBloodRhs(),
                'required' => false,'expanded' =>false,'multiple'=>false,'empty_data' =>9,
                'attr' => ['class'=>'selector skip_these']])



            ->add('Update', 'submit', ['attr' => ['class' => 'btn btn-primary btn-lg']]);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'VswSystem\SecurityBundle\Entity\UserProfile',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'user_profile_edit';
    }


}

