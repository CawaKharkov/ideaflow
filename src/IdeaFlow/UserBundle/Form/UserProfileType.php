<?php

namespace IdeaFlow\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserProfileType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('country','country',['required' => false])
            ->add('city','text',['required' => false])
            ->add('age','text',['required' => false])
            ->add('interests','text',['required' => false])
            ->add('cover','user_profile_cover',['required' => false])
            ->add('submit', 'submit');
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'IdeaFlow\UserBundle\Entity\UserProfile'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'user_profile';
    }
}

