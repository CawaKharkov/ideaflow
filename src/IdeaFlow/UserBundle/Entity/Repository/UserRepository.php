<?php

namespace IdeaFlow\UserBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use IdeaFlow\AppBundle\Entity\Traits\GetCount;


/**
 * UserRepository
 */
class UserRepository extends EntityRepository
{

   use GetCount;
}

