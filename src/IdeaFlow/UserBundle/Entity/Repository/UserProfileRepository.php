<?php

namespace IdeaFlow\UserBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * UserProfileRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class UserProfileRepository extends EntityRepository
{

    public function findFavourites(array $favourites)
    {
        $qb = $this->createQueryBuilder('p')
            ->select('i');
            $qb->join('IdeaFlow\IdeaBundle\Entity\Idea', 'i','WITH','i.id IN (:favourites)')
            ->setParameter('favourites',$favourites);
        //$qb


        /*$qb->orWhere('i.title LIKE :query')
            ->OrWhere('i.text LIKE :query')
            //  $qb->andWhere($qb->expr()->eq('c.name',$category->getName()));
            ->setParameter('category', $category)
            ->setParameter('query', '%' . $query . '%');*/


        return $qb->getQuery()->getResult();
    }

    public function findFavouritesByUser(array $favourites)
    {
        $qb = $this->createQueryBuilder('p')
            ->select('i');
            $qb->join('IdeaFlow\IdeaBundle\Entity\Idea', 'i','WITH','i.id IN (:favourites)')
            ->setParameter('favourites',$favourites);
        //$qb


        /*$qb->orWhere('i.title LIKE :query')
            ->OrWhere('i.text LIKE :query')
            //  $qb->andWhere($qb->expr()->eq('c.name',$category->getName()));
            ->setParameter('category', $category)
            ->setParameter('query', '%' . $query . '%');*/


        return $qb->getQuery()->getResult();
    }

}

