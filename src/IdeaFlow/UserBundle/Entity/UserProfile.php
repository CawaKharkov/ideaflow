<?php

namespace IdeaFlow\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use IdeaFlow\AppBundle\Entity\Traits\CoveredEntity;
use IdeaFlow\AppBundle\Entity\Traits\IdentificationalEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * UserProfile
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="IdeaFlow\UserBundle\Entity\Repository\UserProfileRepository")
 */
class UserProfile
{

    use IdentificationalEntity;
    use TimestampableEntity;
    use CoveredEntity;

    /**
     * @var UserProfile
     * @ORM\OneToOne(targetEntity="User", mappedBy="profile", cascade={"persist", "remove"})
     */
    protected $user;


    /**
     * @var
     * @ORM\Column(name="country", type="string",length=25, nullable=true)
     */
    protected $country;

    /**
     * @var
     * @ORM\Column(name="city", type="string",length=25, nullable=true)
     */
    protected $city;


    /**
     * @ORM\Column(name="age", type="smallint", nullable=true)
     * Assert\Type(type="integer", message="The value {{ value }} is not a valid {{ type }}.")
     */
    protected $age;


    /**
     * @var
     * @ORM\OneToOne(targetEntity="IdeaFlow\UserBundle\Entity\ProfileCover",
     *         orphanRemoval=true,cascade={"persist"})
     * @Assert\Valid()
     */
    protected $cover;


    /**
     * @var int[]
     * @ORM\Column(name="favourites",type="array"),
     */
    protected $favourites = 'a:0:{}';


    /**
     * @var
     * @ORM\Column(name="interests", type="string",length=255, nullable=true)
     */
    protected $interests;

    /**
     * @return mixed
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @param mixed $age
     */
    public function setAge($age)
    {
        $this->age = $age;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }


    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getInterests()
    {
        return $this->interests;
    }

    /**
     * @param mixed $interests
     */
    public function setInterests($interests)
    {
        $this->interests = $interests;
    }


    /**
     * @param \IdeaFlow\UserBundle\Entity\UserProfile $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return \IdeaFlow\UserBundle\Entity\UserProfile
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return \int[]
     */
    public function getFavourites()
    {
        return $this->favourites;
    }

    /**
     * @param \int[] $favourites
     */
    public function setFavourites($favourites)
    {
        $this->favourites = $favourites;
        return $this;
    }

    public function addFavourites($favourite)
    {
        $fav = $this->getFavourites();
        if (!in_array($favourite, $fav)) {
            array_push($fav, $favourite);
            $this->setFavourites($fav);
        }
        return $this;

    }

    public function removeFavourites($favourite)
    {
        $fav = $this->getFavourites();

        $key = array_search($favourite, $fav);
        if ($key !== false) {
            unset($fav[$key]);
        }

        $this->setFavourites($fav);
        return $this;
    }


}

