<?php

namespace IdeaFlow\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use IdeaFlow\AppBundle\Entity\Traits\IdentificationalEntity;
use IdeaFlow\AppBundle\Entity\Traits\IsActiveEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * User
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="IdeaFlow\UserBundle\Entity\Repository\UserRepository")
 * @ORM\HasLifecycleCallbacks
 * @UniqueEntity(fields="username", message="Sorry, this username is already taken.", groups={"signup"})
 * @UniqueEntity(fields="email", message="Sorry, this email is already taken.", groups={"signup"})
 */
class User implements AdvancedUserInterface, \Serializable
{

    const DEFAULT_ROLE = 0;

    protected static $roles = [0 => 'ROLE_USER', 1 => 'ROLE_MODERATOR', 2 => 'ROLE_ADMIN',
        3 => 'ROLE_SUPER_ADMIN'];
    protected $roleName;

    use IdentificationalEntity;
    use TimestampableEntity;
    use IsActiveEntity;
    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=25, unique=true)
     * @Assert\NotBlank(groups={"signup","login"})
     * @Assert\Length(
     *      groups={"signup", "login"},
     *      min = 2,
     *      max = 25,
     *      minMessage = "Your username name must be at least {{ limit }} characters long",
     *      maxMessage = "Your username name cannot be longer than {{ limit }} characters long"
     * )
     */
    protected $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255)
     * @Assert\NotBlank(groups={"signup", "login"})
     * @Assert\Length(
     *      groups={"signup", "login"},
     *      min = 6,
     *      max = 25,
     *      minMessage = "Your password name must be at least {{ limit }} characters long",
     *      maxMessage = "Your password name cannot be longer than {{ limit }} characters long"
     * )
     */
    protected $password;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=50)
     * @Assert\NotBlank(groups={"signup"})
     * @Assert\Email(
     *     groups={"signup"},
     *     message = "The email '{{ value }}' is not a valid email.",
     *     checkMX = true
     * )
     */
    protected $email;


    /**
     * @ORM\Column(name="role", type="integer")
     */
    protected $role = self::DEFAULT_ROLE;


    /**
     * @var UserProfile
     * @ORM\OneToOne(targetEntity="UserProfile", inversedBy="user", cascade={"persist", "remove"})
     */
    protected $profile;

    /**
     * Set username
     *
     * @param string $name
     * @return User
     */
    public function setUsername($name)
    {
        $this->username = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getCover()
    {
        return $this->cover;
    }

    /**
     * @param mixed $cover
     */
    public function setCover($cover)
    {
        $this->cover = $cover;
    }



    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        if ($this->isActive === null) {
            $this->isActive = 1;
        }
    }

    /**
     * @inheritDoc
     */
    public function getSalt()
    {
        // you *may* need a real salt depending on your encoder
        // see section on salt below
        return null;
    }

    /**
     * @inheritDoc
     */
    public function getRoles()
    {
        return [self::$roles[$this->role]];
    }

    public function setRole($role)
    {
        $this->role = in_array($role, self::$roles) || array_key_exists($role, self::$roles)
            ? $role
            : self::DEFAULT_ROLE;
    }

    public function getRole()
    {
        return $this->role;
    }

    public function getRoleName()
    {
        return self::$roles[$this->role];
    }

    public static function getRoleList()
    {
        return self::$roles;
    }


    /**
     * @inheritDoc
     */
    public function eraseCredentials()
    {

    }

    /**
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt,
        ));
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt
            ) = unserialize($serialized);
    }

    /**
     * @param \IdeaFlow\UserBundle\Entity\UserProfile $profile
     */
    public function setProfile($profile)
    {
        $this->profile = $profile;
    }

    /**
     * @return \IdeaFlow\UserBundle\Entity\UserProfile
     */
    public function getProfile()
    {
        return $this->profile;
    }


    /**
     * Checks whether the user's account has expired.
     *
     * Internally, if this method returns false, the authentication system
     * will throw an AccountExpiredException and prevent login.
     *
     * @return bool    true if the user's account is non expired, false otherwise
     *
     * @see AccountExpiredException
     */
    public function isAccountNonExpired()
    {
        return true;
    }

    /**
     * Checks whether the user is locked.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a LockedException and prevent login.
     *
     * @return bool    true if the user is not locked, false otherwise
     *
     * @see LockedException
     */
    public function isAccountNonLocked()
    {
        return true;
    }

    /**
     * Checks whether the user's credentials (password) has expired.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a CredentialsExpiredException and prevent login.
     *
     * @return bool    true if the user's credentials are non expired, false otherwise
     *
     * @see CredentialsExpiredException
     */
    public function isCredentialsNonExpired()
    {
        return true;
    }

    /**
     * Checks whether the user is enabled.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a DisabledException and prevent login.
     *
     * @return bool    true if the user is enabled, false otherwise
     *
     * @see DisabledException
     */
    public function isEnabled()
    {
       return true;
    }
}

