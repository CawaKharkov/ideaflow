<?php

namespace IdeaFlow\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use IdeaFlow\IdeaBundle\Entity\AbstractEntity\File;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * IdeaCover
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="IdeaFlow\UserBundle\Entity\Repository\ProfileCoverRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ProfileCover extends File
{
    protected $uploadPath = '/uploads/user/profile/cover';

    /**
     * @Assert\Image(
     *     maxSize="600k",
     *     minWidth = 100,
     *     maxWidth = 1000,
     *     minHeight = 100,
     *     maxHeight = 1000
     * )
     */
    protected $file;

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }



}

