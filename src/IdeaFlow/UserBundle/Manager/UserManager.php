<?php

namespace VswSystem\SecurityBundle\Manager;

use VswSystem\CoreBundle\Manager\AbstractManager;

/**
 * Description of UserManager
 *
 * @author cawa
 */
class UserManager extends AbstractManager
{

    protected $userEntity;
    protected $subscriberEntity;

    public function getUserRepository()
    {
        return $this->getRepository($this->getUserEntity());
    }

    public function setUserEntity($entity)
    {
        $this->userEntity = $entity;
    }

    /**
     * @return mixed
     */
    public function getUserEntity()
    {
        return $this->userEntity;
    }


    public function getUsers()
    {
        return $this->getUserRepository()->findAll();
    }

    /**
     * @param mixed $subscriberEntity
     */
    public function setSubscriberEntity($subscriberEntity)
    {
        $this->subscriberEntity = $subscriberEntity;
    }

    /**
     * @return mixed
     */
    public function getSubscriberEntity()
    {
        return $this->subscriberEntity;
    }

    public function getSubscriberRepository()
    {
        return $this->getRepository($this->getSubscriberEntity());
    }
}

