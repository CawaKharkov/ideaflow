<?php

namespace IdeaFlow\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class AbstractController
 * @package IdeaFlow\UserBundle\Controller
 */
class AbstractController extends Controller
{
    /**
     * @return \IdeaFlow\UserBundle\Service\UserService
     */
    protected function getUserService()
    {
        return $this->get('ideaflow.orm.service.user');
    }

    /**
     * @return \IdeaFlow\UserBundle\Service\UserProfileService
     */
    protected function getUserProfileService()
    {
        return $this->get('ideaflow.orm.service.user.profile');
    }
} 
