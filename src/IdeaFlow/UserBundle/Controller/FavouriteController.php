<?php

namespace IdeaFlow\UserBundle\Controller;

use IdeaFlow\UserBundle\Controller\AbstractController as UserAbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;


/**
 * @Route("/user/favourite")
 */
class FavouriteController extends UserAbstractController
{

    /**
     * @Route("/{id}", name="favourite_by_user")\
     * @ParamConverter("idea", class="IdeaFlowIdeaBundle:Idea")
     * @Security("is_granted('ROLE_USER')")
     * @Template()
     */
    public function favouriteByAction(Request $request, $idea)
    {

        $favourites = $this->getUser()->getProfile()->getFavourites();
        $favouriteIdeas = $this->get('ideaflow.orm.service.user.profile')->getRepository()->findFavouritesByUser($favourites);

        return [];
    }

}

