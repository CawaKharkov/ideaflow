<?php

namespace IdeaFlow\UserBundle\Controller;

use IdeaFlow\UserBundle\Controller\AbstractController as UserAbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;


/**
 * @Route("/user/")
 */
class UserAccountController extends UserAbstractController
{

    /**
     * @Route( name="user_account")
     * @Security("is_granted('ROLE_USER')")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $profile = $this->getUser()->getProfile();

        $ideas = $this->get('ideaflow.orm.service.idea')->getRepository()->getUserIdeas($this->getUser());

        $pageNumber = $request->query->get('page', 1);

        $pager = $this->get('ideaflow.orm.service.idea')->getPagenated($ideas, $pageNumber, 9);


        $favourites = $this->getUser()->getProfile()->getFavourites();

        $favouriteIdeas = $this->get('ideaflow.orm.service.user.profile')->getRepository()->findFavourites($favourites);

        $favouriteIdeas = $this->get('ideaflow.orm.service.idea')->getPagenated($favouriteIdeas, $pageNumber, 3);

        return ['profile' => $profile, 'ideasCount' => count($ideas), 'ideas' => $pager,'favouriteIdeas' => $favouriteIdeas];
    }

    /**
     * @Route("ideas/", name="user_ideas_pager")
     * @Security("is_granted('ROLE_USER')")
     * @Template()
     */
    public function userIdeasAction(Request $request)
    {
        $ideas = $this->get('ideaflow.orm.service.idea')->getRepository()->getUserIdeas($this->getUser());
        $pageNumber = $request->query->get('page', 1);

        $pager = $this->get('ideaflow.orm.service.idea')->getPagenated($ideas, $pageNumber);

        return ['ideas' => $pager];
    }

    /**
     * @Route("ideasFavourite/", name="user_favourites_ideas_pager")
     * @Security("is_granted('ROLE_USER')")
     * @Template()
     */
    public function userFavouritesIdeasAction(Request $request)
    {
        $favourites = $this->getUser()->getProfile()->getFavourites();

        $pageNumber = $request->query->get('page', 1);

        $favouriteIdeas = $this->get('ideaflow.orm.service.user.profile')->getRepository()->findFavourites($favourites);

        $favouriteIdeas = $this->get('ideaflow.orm.service.idea')->getPagenated($favouriteIdeas, $pageNumber, 3);


        return $this->render('IdeaFlowUserBundle:UserAccount:userIdeas.html.twig',['ideas' => $favouriteIdeas]);
    }

    /**
     * @Route("edit/", name="user_account_edit")
     * @Security("is_granted('ROLE_USER')")
     * @Template()
     */
    public function editAction(Request $request)
    {
        $profileForm = $this->createForm('user_profile', $this->getUser()->getProfile());

        $profileForm->handleRequest($request);

        $isCreated = false;
        if ($profileForm->isValid()) {
            $this->getUserProfileService()->save($profileForm->getData(), true);

            $this->addFlash('success', 'User profile updated!');
            $isCreated = true;
        }

        return $isCreated
            ? $this->redirectToRoute('user_account')
            : ['profileForm' => $profileForm->createView()];
    }
}
