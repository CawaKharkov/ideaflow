<?php

namespace IdeaFlow\UserBundle\Controller;

use IdeaFlow\UserBundle\Controller\AbstractController as UserAbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class AuthController extends UserAbstractController
{

    /**
     * @Route("/user/check", name="_security_check")
     */
    public function securityCheckAction()
    {
        // The security layer will intercept this request
    }

    /**
     * @Route("/logout/", name="_user_logout")
     */
    public function logoutAction()
    {
        // The security layer will intercept this request
    }

    /**
     * @Route("/user/login/", name="_user_login_page")
     * @Template()
     */
    public function loginPageAction(Request $request)
    {
        $loginForm = $this->get('form.factory')
                ->createNamedBuilder(null, 'form')
                ->setAction($this->generateUrl('_security_check'))
                ->add('_username', 'text', ['required' => true, 'label' => 'Username or email',
                    'attr' => ['class' => '',
                       ]])
                ->add('_password', 'password', ['required' => true, 'label' => 'Password',
                    'attr' => ['class' => '','style'=>'',
                       ]])
                ->add('_remember_me', 'checkbox', ['required' => false])
                ->add('login', 'submit', ['label' => 'Login',
                    'attr' => ['class' => '']])
                ->getForm();
        /* $this->get('session')->getFlashBag()->add(
          'info',
          'Your changes were saved!'
          ); */
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = $request->getSession()->get(SecurityContext::AUTHENTICATION_ERROR);
        }
        return ['loginForm' => $loginForm->createView(), 'error' => $error];
    }

}

