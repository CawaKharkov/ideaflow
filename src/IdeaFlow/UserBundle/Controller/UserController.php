<?php

namespace IdeaFlow\UserBundle\Controller;

use IdeaFlow\UserBundle\Controller\AbstractController as UserAbstractController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use IdeaFlow\UserBundle\Entity\User;

class UserController extends AbstractController
{

    /**
     * @Route("/user/signup/", name="_user_signup")
     * @Template()
     */
    public function signupAction(Request $request)
    {
        $signupForm = $this->createForm('user_signup', new User(),[]);
        $signupForm->handleRequest($request);

        $isCreated = false;

        if ($signupForm->isValid()) {
            $this->get('ideaflow.security.registration')->registerUser($signupForm->getData(),true);
            $isCreated = true;
        }

        return $isCreated
                ? $this->redirect($this->generateUrl('homepage'))
                : ['signupForm' => $signupForm->createView()];
    }

}

