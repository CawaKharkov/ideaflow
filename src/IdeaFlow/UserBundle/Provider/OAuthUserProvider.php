<?php

namespace IdeaFlow\UserBundle\Provider;

use HWI\Bundle\OAuthBundle\Security\Core\User\OAuthUserProvider as BaseOAuthUserProvider;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use Doctrine\ORM\EntityManager;
use VswSystem\SecurityBundle\Registration\RegistrationService;
use VswSystem\SecurityBundle\Entity\User;

class OAuthUserProvider extends BaseOAuthUserProvider
{

    protected $em;
    protected $entity = 'IdeaFlow\UserBundle\Entity\User';
    protected $registrastionService;

    public function __construct(EntityManager $em,  $registrastionService)
    {
        $this->em = $em;
        $this->registrastionService = $registrastionService;
    }

    /**
     * {@inheritdoc}
     */
    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {
       /* var_dump($response);

        $consumerKey = 'RDSf2TLtCdrqsWVAQKnO701Np';
        $consumerSecret = 'WrAeiJFdgfCdEG3DwwdtLJ1n2K7l9nV5Ra5E9GdlLjMF35ZAZa';
        $accessToken = $response->getAccessToken();
        $accessTokenSecret = $response->getTokenSecret();

        $twitter = new \TwitterPhp\RestApi($consumerKey,$consumerSecret,$accessToken,$accessTokenSecret);

        /*
         * Connect as application
         * https://dev.twitter.com/docs/auth/application-only-auth
         */
       // $connection = $twitter->connectAsApplication();

        /*
         * Collection of the most recent Tweets posted by the user indicated by the screen_name, without replies
         * https://dev.twitter.com/docs/api/1.1/get/statuses/user_timeline
         */
    ////    $lastest = $connection->get('/statuses/user_timeline',array('screen_name' => 'CawaKharkov', 'exclude_replies' => 'true','count'=>2));
      //  var_dump($lastest); die();*/
        return $this->loadUserByOAuthResponse($response->getEmail(), $response->getRealName(), $response);
    }

    public function loadUserByOAuthResponse($username, $name,UserResponseInterface $response)
    {
        $query = $this->em->createQuery('SELECT u FROM IdeaFlow\UserBundle\Entity\User u '
                . 'WHERE u.username = :username OR u.email = :email');
        $query->setParameters(['username' => $username, 'email' => $username]);

        try {
            // The Query::getSingleResult() method throws an exception
            // if there is no record matching the criteria.
            $user = $query->getOneOrNullResult();
        } catch (NoResultException $e) {
            $message = sprintf(
                    'Unable to find an active admin ApplicationNotepadByndle:User object identified by "%s".', $username
            );
            throw new UsernameNotFoundException($message, 0, $e);
        }

        if (!$user) {

            $user = new User();
            $user->setEmail($username);
            $user->setUsername($name);
            $user->setRole($user::DEFAULT_ROLE);
            $this->registrastionService->registerUser($user, true, true);
        }
        return $user;
    }

}

