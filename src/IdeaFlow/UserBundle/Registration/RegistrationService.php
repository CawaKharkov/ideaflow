<?php

namespace IdeaFlow\UserBundle\Registration;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\QueryException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use IdeaFlow\UserBundle\Entity\UserProfile;

/**
 * Class for user registration methods
 * 
 * @author cawa
 */
class RegistrationService implements ContainerAwareInterface
{

    protected $em;
    protected $entity = 'IdeaFlow\UserBundle\Entity\User';
    private $container;

    /**
     * Create RegistrationService and set em
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Inject DI Container
     * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * Create user
     * If $login == true, login
     * @param \Symfony\Component\Security\Core\User\UserInterface $user
     * @param bool $login
     */
    public function registerUser(UserInterface $user, $login = false, $sendInfo = false)
    {
        $factory = $this->container->get('security.encoder_factory');

        $encoder = $factory->getEncoder($user);
        $password = $encoder->encodePassword($user->getPassword(), $user->getSalt());

        $user->setPassword($password);

        $user->setProfile($this->createUserProfile($user));
        $this->em->persist($user);
        $this->em->flush();

        if ($login) {
            $this->afterRegister($user);
        }
        if ($sendInfo) {
            $this->sendInfo($user);
        }


    }

    /**
     * Set security context
     * @param \Symfony\Component\Security\Core\User\UserInterface $user
     */
    protected function afterRegister(UserInterface $user)
    {
        $token = new UsernamePasswordToken($user, null, 'secured_area', $user->getRoles());
        $this->container->get('security.context')->setToken($token);
    }

    protected function sendInfo(UserInterface $user)
    {
        $message = \Swift_Message::newInstance()
                ->setSubject('Hello Email')
                ->setFrom('cawa123@mail.ru')
                ->setTo($user->getEmail())
                ->setBody('You have registred user, username:' . $user->getUsername()
                . ', email:' . $user->getEmail())
        ;
        $this->container->get('mailer')->send($message);
    }


    public function createUserProfile(UserInterface $user)
    {
        $profile = new UserProfile();
        $profile->setUser($user);

        $this->em->persist($profile);
        $this->em->flush();

        return $profile;
    }
}

