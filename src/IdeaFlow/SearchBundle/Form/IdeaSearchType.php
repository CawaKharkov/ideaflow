<?php

namespace IdeaFlow\SearchBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class IdeaSearchType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //   ->add('title')
            ->add('text', 'text', ['label' => false,
                'required' => false,
                'attr' => ['placeholder' => 'Enter query']])
            //    ->add('cover', 'idea_cover',['required' => false])
            ->add('categories', 'entity', [//'type' => 'idea_category',
                'label' => false, 'empty_value' => 'Or select category',
                'required' => false,
                'class' => 'IdeaFlowIdeaBundle:IdeaCategory',
                'property' => 'name',
                'expanded' => false,
                'multiple' => false,
            ]);
           // ->add('search', 'submit');
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(//     'data_class' => 'IdeaFlow\IdeaBundle\Entity\Idea'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'idea_search_type';
    }
}

