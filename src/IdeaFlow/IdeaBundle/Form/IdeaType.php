<?php

namespace IdeaFlow\IdeaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use IdeaFlow\IdeaBundle\Entity\Idea;

class IdeaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('text')
            ->add('cover', 'idea_cover',['required' => false])
            ->add('categories', 'entity', [//'type' => 'idea_category',
                'class' => 'IdeaFlowIdeaBundle:IdeaCategory',
                'property' => 'name',
                'expanded' => true,
                'multiple' => true,
            ])
            ->add('complexity','choice',['choices' => Idea::getComplexityLevels()])
            ->add('submit', 'submit');
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'IdeaFlow\IdeaBundle\Entity\Idea'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'idea_create_type';
    }
}

