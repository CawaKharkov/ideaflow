<?php

namespace IdeaFlow\IdeaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class IdeaCoverType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // $listener = new SettingsDataListener($builder->getFormFactory());
        //     $builder->addEventSubscriber($listener);
        //   if ($this->isNew) {
        $builder->add('file', 'file', ['label' => false, 'required' => false]);
        //    }
     /*   $builder->add('name', 'text', [
            'required' => false, 'label' => 'Image name', 'data' => 'image' . uniqid()]); */


    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'IdeaFlow\IdeaBundle\Entity\IdeaCover'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'idea_cover';
    }
}

