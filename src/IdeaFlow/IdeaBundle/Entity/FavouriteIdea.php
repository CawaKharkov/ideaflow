<?php

namespace IdeaFlow\IdeaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FavouriteIdea
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="IdeaFlow\IdeaBundle\Entity\FavouriteIdeaRepository")
 */
class FavouriteIdea
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="user", type="string", length=255)
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="idea", type="string", length=255)
     */
    private $idea;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param string $user
     * @return FavouriteIdea
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return string 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set idea
     *
     * @param string $idea
     * @return FavouriteIdea
     */
    public function setIdea($idea)
    {
        $this->idea = $idea;

        return $this;
    }

    /**
     * Get idea
     *
     * @return string 
     */
    public function getIdea()
    {
        return $this->idea;
    }
}

