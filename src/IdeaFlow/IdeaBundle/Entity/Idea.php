<?php

namespace IdeaFlow\IdeaBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use IdeaFlow\AppBundle\Entity\Traits\CoveredEntity;
use IdeaFlow\AppBundle\Entity\Traits\IdentificationalEntity;
use IdeaFlow\AppBundle\Entity\Traits\TitledEntity;
use IdeaFlow\IdeaBundle\Entity\Traits\CategorizedEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Idea
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="IdeaFlow\IdeaBundle\Entity\Repository\IdeaRepository")
 */
class Idea
{

    use IdentificationalEntity;
    use TitledEntity;
    use CategorizedEntity;
    use CoveredEntity;
    use TimestampableEntity;

    /**
     * @var
     * @ORM\OneToOne(targetEntity="IdeaFlow\IdeaBundle\Entity\IdeaCover",
     *          inversedBy="idea",orphanRemoval=true,cascade={"persist"})
     * @Assert\Valid()
     */
    protected $cover;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text")
     * @Assert\NotBlank()
     */
    protected $text;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="IdeaFlow\UserBundle\Entity\User",cascade={"persist", "remove"})
     */
    protected $user;


    public function __construct()
    {
        $this->categories = new ArrayCollection();
    }


    /**
     * @var int
     * @ORM\Column(name="complexity",type="smallint")
     */
    protected $complexity = 0;

    protected static $complexityLevels = [0 => 'Easy', 1 => 'Normal', 2 => 'Hard'];

    /**
     * Set text
     *
     * @param string $text
     * @return Idea
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return int
     */
    public function getComplexity()
    {
        return $this->complexity;
    }

    /**
     * @param int $complexity
     */
    public function setComplexity($complexity)
    {
        $this->complexity = $complexity;
    }

    /**
     * @return array
     */
    public static function getComplexityLevels()
    {
        return self::$complexityLevels;
    }

    /**
     * @return string
     */
    public function getComplexityLevel()
    {
        return self::$complexityLevels[$this->getComplexity()];
    }


}

