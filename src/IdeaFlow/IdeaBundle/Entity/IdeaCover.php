<?php

namespace IdeaFlow\IdeaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use IdeaFlow\IdeaBundle\Entity\AbstractEntity\File;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * IdeaCover
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="IdeaFlow\IdeaBundle\Entity\Repository\IdeaCoverRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class IdeaCover extends File
{
    protected $uploadPath = '/uploads/ideas/cover';

    /**
     * @Assert\Image(
     *     maxSize="1000k",
     *     minWidth = 50,
     *     maxWidth = 2000,
     *     minHeight = 50,
     *     maxHeight = 2000
     * )
     */
    protected $file;

    /**
     * @var
     * @ORM\OneToOne(targetEntity="IdeaFlow\IdeaBundle\Entity\Idea",mappedBy="cover")
     */
    protected $idea;

    /**
     * @return mixed
     */
    public function getIdea()
    {
        return $this->idea;
    }

    /**
     * @param mixed $idea
     */
    public function setIdea($idea)
    {
        $this->idea = $idea;
    }


}

