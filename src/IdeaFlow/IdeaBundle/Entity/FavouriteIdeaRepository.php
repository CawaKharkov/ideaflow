<?php

namespace IdeaFlow\IdeaBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * FavouriteIdeaRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class FavouriteIdeaRepository extends EntityRepository
{
}

