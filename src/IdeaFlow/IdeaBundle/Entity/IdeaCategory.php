<?php

namespace IdeaFlow\IdeaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use IdeaFlow\AppBundle\Entity\Traits\IdentificationalEntity;
use IdeaFlow\AppBundle\Entity\Traits\IsActiveEntity;
use IdeaFlow\AppBundle\Entity\Traits\NamedEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * IdeaCategory
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="IdeaFlow\IdeaBundle\Entity\Repository\IdeaCategoryRepository")
 * @UniqueEntity(fields="name", message="Sorry, this category name is already taken.")
 */
class IdeaCategory
{
    use IdentificationalEntity;
    use NamedEntity;
    use IsActiveEntity;


}

