<?php

namespace IdeaFlow\IdeaBundle\Entity\Traits;


use IdeaFlow\IdeaBundle\Entity\IdeaCategory as Category;

/**
 * Class CategorizedEntity
 * @package IdeaFlow\IdeaBundle\Entity\Traits
 */
trait CategorizedEntity
{

    /**
     * @var \IdeaFlow\IdeaBundle\Entity\IdeaCategory[]
     * @ORM\ManyToMany(targetEntity="IdeaFlow\IdeaBundle\Entity\IdeaCategory")
     */
    protected $categories;

    /**
     * @return mixed
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param mixed $categories
     */
    public function setCategories($categories)
    {
        $this->categories = $categories;
    }

    /**
     * @param Category $category
     * @return $this
     */
    public function addCategory(Category $category)
    {
        if(!$this->categories->contains($category)){
            $this->categories->add($category);
        }
        return $this;
    }

    /**
     * @param Category $category
     * @return $this
     */
    public function removeCategory(Category $category)
    {
        $this->categories->removeElement($category);
        return $this;
    }
} 
