<?php

namespace IdeaFlow\IdeaBundle\Controller;

use IdeaFlow\IdeaBundle\Controller\AbstractController as IdeaAbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/idea/search")
 */
class SearchController extends IdeaAbstractController
{
    /**
     * @Route("/",name="ideas_search")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $pageNumber = $request->query->get('page', 1);

        $searchFrom = $this->createForm('idea_search_type',null,['action' => $this->generateUrl('ideas_search')]);
        $searchFrom->handleRequest($request);

        $ideas = [];

        if($searchFrom->isValid()){

            $ideas = $this->getIdeaService()->getRepository()
                ->findByQuery($searchFrom->getData()['text'],$searchFrom->getData()['categories']);
        }

        $ideas = $this->getIdeaService()->getPagenated($ideas,$pageNumber);

        return ['ideas' => $ideas];
    }


}

