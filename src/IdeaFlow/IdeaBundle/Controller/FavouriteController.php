<?php

namespace IdeaFlow\IdeaBundle\Controller;

use IdeaFlow\IdeaBundle\Controller\AbstractController as IdeaAbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/idea/favourite")
 */
class FavouriteController extends IdeaAbstractController
{
    /**
     * @Route("/add/{id}",name="add_favourite_idea")
     * @Security("is_granted('ROLE_USER')")
     * @Template()
     */
    public function addIdeaAction(Request $request, $id)
    {
        $favourites = $this->getUser()->getProfile()->addFavourites($id);

        $this->get('ideaflow.orm.service.user.profile')->save($favourites,true);

        $referer = $request->headers->get('referer');
        return new RedirectResponse($referer);
    }

    /**
     * @Route("/remove/{id}",name="remove_favourite_idea")
     * @Security("is_granted('ROLE_USER')")
     * @Template()
     */
    public function removeIdeaAction(Request $request, $id)
    {
        $favourites = $this->getUser()->getProfile()->removeFavourites($id);

        $this->get('ideaflow.orm.service.user.profile')->save($favourites,true);

        $referer = $request->headers->get('referer');
        return new RedirectResponse($referer);
    }


}

