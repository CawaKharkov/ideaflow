<?php

namespace IdeaFlow\IdeaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class AbstractController
 * @package IdeaFlow\IdeaBundle\Controller
 */
class AbstractController extends Controller
{
    /**
     * @return \IdeaFlow\IdeaBundle\Service\IdeaService
     */
    protected  function getIdeaService()
    {
        return $this->get('ideaflow.orm.service.idea');
    }

    /**
     * @return \IdeaFlow\IdeaBundle\Service\IdeaCategoryService
     */
    protected function getIdeaCategotyService()
    {
        return $this->get('ideaflow.orm.service.idea.category');
    }
}

