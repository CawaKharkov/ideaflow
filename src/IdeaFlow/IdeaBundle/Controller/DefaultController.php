<?php

namespace IdeaFlow\IdeaBundle\Controller;

use IdeaFlow\IdeaBundle\Controller\AbstractController as IdeaAbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/idea")
 */
class DefaultController extends IdeaAbstractController
{
    /**
     * @Route("/",name="ideas")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $pageNumber = $request->query->get('page', 1);

        $ideas = $this->getIdeaService()->getPagenated(null, $pageNumber);

        return ['ideas' => $ideas];
    }

    /**
     * @Route("/{name}", name="ideas_by_category")
     * @ParamConverter("category", class="IdeaFlowIdeaBundle:IdeaCategory")
     * @Template()
     */
    public function byCategoryAction(Request $request, $category)
    {
        $ideas = $this->getIdeaService()->getRepository()->findAllByCategory($category);

        $pageNumber = $request->query->get('page', 1);

        $paginated = $this->getIdeaService()->getPagenated($ideas, $pageNumber);

        return $this->render('IdeaFlowIdeaBundle:Default:index.html.twig',
            ['ideas' => $paginated, 'category' => $category]);
    }

    /**
     * @Route("/submit/",name="idea_submit")
     * @Security("is_granted('ROLE_USER')")
     * @Template()
     */
    public function submitIdeaAction(Request $request)
    {
        $ideaForm = $this->createForm('idea_create_type', $this->getIdeaService()->create());

        $ideaForm->handleRequest($request);

        $isCreated = false;
        if ($ideaForm->isValid()) {
            $this->getIdeaService()->save($ideaForm->getData(), true);
            $this->addFlash('success', 'You have created new idea!');
            $isCreated = true;
        }

        return $isCreated
            ? $this->redirectToRoute('ideas')
            : ['ideaForm' => $ideaForm->createView()];
    }


    /**
     * @Route("/view/{id}",name="view_idea")
     * @ParamConverter("idea", class="IdeaFlowIdeaBundle:Idea")
     * @Template()
     */
    public function viewAction($idea)
    {
        $user = $this->getUser();
        $isFavorite = false;

        if ($user) {
            $favourites = $user->getProfile()->getFavourites();
            if (in_array($idea->getId(), $favourites)) {
                $isFavorite = true;
            }
        }


        return ['idea' => $idea, 'isFavorite' => $isFavorite];
    }

}

