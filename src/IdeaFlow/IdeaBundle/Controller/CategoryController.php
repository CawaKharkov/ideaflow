<?php

namespace IdeaFlow\IdeaBundle\Controller;

use IdeaFlow\IdeaBundle\Controller\AbstractController as IdeaAbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/category")
 */
class CategoryController extends IdeaAbstractController
{
    /**
     * @Route("/list",name="ideas_categories")
     * @Template()
     */
    public function getListAction($current = null)
    {
        $categories = $this->getIdeaCategotyService()->findAllArray();

        return ['categories' => $categories,'current' => $current];
    }

    /**
     * @Route("/submit/",name="category_submit")
     * @Security("is_granted('ROLE_USER')")
     * @Template()
     */
    public function submitCategoryAction(Request $request)
    {
        $categoryForm = $this->createForm('idea_category_submit',$this->getIdeaCategotyService()->create());

        $categoryForm->handleRequest($request);

        $isCreated = false;
        if ($categoryForm->isValid()) {
            $this->getIdeaCategotyService()->save($categoryForm->getData(), true);
            $this->addFlash('success', 'You have submitted new category!');
            $isCreated = true;
        }

        return $isCreated
            ? $this->redirectToRoute('ideas')
            : ['categoryForm' => $categoryForm->createView()];
    }

}
