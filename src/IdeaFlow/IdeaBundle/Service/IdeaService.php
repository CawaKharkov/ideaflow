<?php

namespace IdeaFlow\IdeaBundle\Service;

use IdeaFlow\AppBundle\Service\AbstractEntityService;

/**
 * Class IdeaService
 * @package IdeaFlow\IdeaBundle\Service
 */
class IdeaService extends AbstractEntityService
{
    protected $user;
    protected $pager;

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getPager()
    {
        return $this->pager;
    }

    /**
     * @param mixed $pager
     */
    public function setPager($pager)
    {
        $this->pager = $pager;
    }


    /**
     * @param $objects
     * @param $pageNumber
     * @param int $count
     * @return $objects[]
     */
    public function getPagenated($objects = null, $pageNumber, $count = 9)
    {
        if($objects === null){
            $objects = $this->findAll();
        }

        return $this->getPager()->getPagination($objects, $pageNumber, $count);

    }


    /**
     * Save entity in databse
     * @param Entity $entity
     * @return Entity
     */
    public function save($entity, $flush = false)
    {
        $entity->setUser($this->getUser());

        parent::save($entity, $flush);
    }

} 
