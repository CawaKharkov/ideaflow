<?php

namespace IdeaFlow\AppBundle\Pagination;


use Knp\Bundle\PaginatorBundle\KnpPaginatorBundle;
use Knp\Component\Pager\Paginator;
use Knp\Component\Pager\PaginatorInterface;

/**
 * Class BasePagination
 * @package IdeaFlow\AppBundle\Pagination
 */
class BasePagination
{

    protected $pagerObject;
    protected $items;

    public function __construct(Paginator $pager)
    {

        $this->pagerObject = $pager;
    }

    /**
     * @return \Knp\Component\Pager\Pagination\PaginationInterface
     */
    public function getPagerObject()
    {
        return $this->pagerObject;
    }



    public function getPagination($items,$page,$limit = 10)
    {
        $pagination = $this->getPagerObject()->paginate(
            $items,
            $page/*page number*/,
            $limit/*limit per page*/
        );

        return $pagination;
    }

} 
