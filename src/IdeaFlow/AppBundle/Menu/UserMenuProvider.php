<?php

namespace IdeaFlow\AppBundle\Menu;

use Knp\Menu\FactoryInterface;
use Knp\Menu\Loader\LoaderInterface;
use Knp\Menu\Provider\MenuProviderInterface;
use VswSystem\CoreBundle\Manager\NavigationManager;

/**
 * Description of UserMenuProvider
 *
 * @author cawa
 */
class UserMenuProvider implements MenuProviderInterface
{

    /**
     * @var FactoryInterface
     */
    protected $factory = null;

    /**
     * @var NavigationManager
     */
    protected $manager = null;

    /**
     * @var LoaderInterface
     */
    protected $loader = null;

    /**
     * @param FactoryInterface $factory the menu factory used to create the menu item
     */
    public function __construct(FactoryInterface $factory, NavigationManager $manager, LoaderInterface $loader)
    {
        $this->factory = $factory;
        $this->manager = $manager;
        $this->loader = $loader;
    }

    /**
     * Retrieves a menu by its name
     *
     * @param string $name
     * @param array $options
     * @return \Knp\Menu\ItemInterface
     * @throws \InvalidArgumentException if the menu does not exists
     */
    public function get($name, array $options = array())
    {
        $customMenu = $this->manager->getMenuByName($name);
        $chidMenus = $customMenu->getChildren();

        if ($customMenu === null) {
            throw new \InvalidArgumentException(sprintf('The menu "%s" is not defined.', $name));
        }

        $menu = $this->factory->createItem($customMenu->getName(), ['navbar' => true]);

        $childLinks = [];
        foreach ($chidMenus as $chidMenu) {
            foreach ($chidMenu->getNavigation() as $subLink) {
               // var_dump($subLink);
                $childLinks[$chidMenu->getConnectorElement()->getId()][]= ['title' => $subLink->getTitle(), 'uri' => $subLink->getRoute()];
            }
        }
       //var_dump($childLinks); die();
        foreach ($customMenu->getNavigation() as $link) {
            $currentLink = $menu->addChild($link->getTitle(), ['uri' => $link->getRoute(),'icon'=>'plus']);
            if(array_key_exists($link->getId(),$childLinks)){
                foreach($childLinks[$link->getId()] as $child){
                    $currentLink->addChild($child['title'], ['uri' => $child['uri']]);
                }

            }
        }
        //$menuItem = $this->factory->createFromNode($menu);
        return $menu;
    }

    /**
     * Checks whether a menu exists in this provider
     *
     * @param string $name
     * @param array $options
     * @return bool
     */
    public function has($name, array $options = array())
    {
        $menu = $this->manager->getMenuByName($name);
        return $menu !== null;
    }

}

