<?php

namespace IdeaFlow\AppBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\ExpressionLanguage\Expression;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\SecurityContextInterface;


class MenuBuilder extends ContainerAware
{

    protected $factory;

    /**
     * @Inject("security.context", required = false)
     */
    protected $securityContext = null;

    /**
     * @param FactoryInterface $factory
     */
    public function __construct(FactoryInterface $factory)
    {
        $this->factory = $factory;
    }

    public function addLoginMenu($menu)
    {
        if ($this->securityContext->isGranted(array(new Expression('!is_authenticated()')))) {
            $menu->addChild('Sign Up', ['route' => '_user_signup']);
            $menu->addChild('Log In', ['route' => '_user_login_page']);

        }
        if ($this->securityContext->isGranted(array(new Expression('is_authenticated()')))) {
            $menu->addChild('Log Out', ['route' => '_user_logout']);
        }
    }


    public function createMainMenu(RequestStack $requestStack)
    {
        $menu = $this->factory->createItem('root');

        $menu->addChild('Home', ['route' => 'homepage']);
        $menu->addChild('Ideas', ['route' => 'ideas']);
        $menu->addChild('How it works', ['route' => 'how_it_works']);

        $this->addLoginMenu($menu);

        //  $this->addUserMenu($menu);

        return $menu;
    }

    public function createFooterMenu(RequestStack $requestStack)
    {
        $menu = $this->factory->createItem('root');

        $menu->addChild('Home', ['route' => 'homepage']);
        $menu->addChild('Ideas', ['route' => 'ideas']);
        $menu->addChild('How it works', ['route' => 'how_it_works']);

        $this->addLoginMenu($menu);

        //  $this->addUserMenu($menu);

        return $menu;
    }


    public function createUserMenu(RequestStack $requestStack)
    {
        $menu = $this->factory->createItem('root');

        $menu->addChild('Submit idea', ['route' => 'idea_submit'])
            ->setAttribute('icon', 'i-shared_recipe');
        $menu->addChild('User account', ['route' => 'user_account'])
            ->setAttribute('icon', 'i-account');
        $menu->addChild('Search ideas', ['route' => 'ideas_search'])
            ->setAttribute('icon', 'i-search');


        return $menu;
    }


    public function createAdminMenu(RequestStack $requestStack)
    {
        $menu = $this->factory->createItem('root', ['navbar' => true]);

        $userMenu = $menu->addChild('Users', ['icon' => 'user',
            'dropdown' => true,
            'caret' => true]);
        $userMenu->addChild('System users', ['route' => 'admin_user', 'icon' => 'user']);
        $userMenu->addChild('Donors', ['route' => 'admin_user_donors', 'icon' => 'user']);

        $nav = $menu->addChild('Navigation', ['route' => 'admin_menu', 'icon' => 'list',
            'dropdown' => true,
            'caret' => true]);
        $nav->addChild('Menus', ['route' => 'admin_menu', 'icon' => 'list-alt']);
        $nav->addChild('Links', ['route' => 'admin_navigation', 'icon' => 'link']);

        $nav = $menu->addChild('Content', ['icon' => 'th',
            'dropdown' => true,
            'caret' => true]);
        $nav->addChild('Content blocks', ['route' => 'admin_content_block', 'icon' => 'tasks']);
        $nav->addChild('Event blocks', ['route' => 'admin_event_block', 'icon' => 'star']);
        $nav->addChild('Questions', ['route' => 'admin_questions', 'icon' => 'question-sign']);
        $nav->addChild('Layout', ['route' => 'admin_layout', 'icon' => 'plus']);
        $nav->addChild('Slider', ['route' => 'admin_slider', 'icon' => 'picture']);
        $nav->addChild('Gallery images', ['route' => 'admin_gallery_group', 'icon' => 'picture']);

        $pages = $menu->addChild('Pages', ['icon' => 'book',
            'dropdown' => true,
            'caret' => true]);
        $pages->addChild('All pages', ['route' => 'admin_pages', 'icon' => 'list']);
        $pages->addChild('Text pages', ['route' => 'admin_text_page', 'icon' => 'file']);
        $pages->addChild('Gallery pages', ['route' => 'admin_gallery_page', 'icon' => 'picture']);
        $pages->addChild('Faq\Conact pages', ['route' => 'admin_faq_page', 'icon' => 'question-sign']);
        $pages->addChild('News\Events pages', ['route' => 'admin_news_page', 'icon' => 'volume-up']);

        $menu->addChild('Settings', ['route' => 'admin_settings', 'icon' => 'wrench']);

        $widgets = $menu->addChild('Widgets', ['icon' => 'book',
            'dropdown' => true,
            'caret' => true]);
        $widgets->addChild('Hall of fame', ['route' => 'admin_widget_hall', 'icon' => 'list']);
        $widgets->addChild('Fast registration', ['route' => 'admin_widget_fast_registration', 'icon' => 'list']);

        return $menu;
    }

    public function createLangMenu()
    {
        $langMenu = $this->factory->createItem('root', ['pull-right' => true, 'navbar' => true]);

        $profile = $langMenu->addChild('User:' . $this->securityContext->getToken()->getUser()->getUserName(), ['icon' => 'briefcase',
            'dropdown' => true,
            'caret' => true]);
        $profile->addChild('User profile', ['route' => 'admin_user_profile', 'icon' => 'heart-empty']);

        $dropdown = $langMenu->addChild('Language', ['dropdown' => true,
            'caret' => true, 'icon' => 'th-large']);

        $dropdown->addChild('EN', ['route' => 'lang_en', 'icon' => 'map-marker',
            'routeParameters' => ['lang' => 'en']]);
        $dropdown->addChild('LT', ['route' => 'lang_lt', 'icon' => 'map-marker',
            'routeParameters' => ['lang' => 'lt']]);


        return $langMenu;
    }

    public function setSecurityContext(SecurityContextInterface $sc = null)
    {
        $this->securityContext = $sc;
    }


    public function addUserMenu($menu)
    {
        if (!$this->securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $menu->addChild('Login', ['route' => '_user_login_page', 'icon' => 'log-in']);
            $menu->addChild('SignUp', ['route' => '_user_signup', 'icon' => 'registration-mark']);
        }

        if ($this->securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $menu->addChild('Logout', ['route' => '_user_logout', 'icon' => 'log-out']);
        }
    }

}

