<?php

namespace IdeaFlow\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


abstract class AbstractController extends Controller
{

    /**
     * @return \VswSystem\CoreBundle\Manager\ContentManager
     */
    protected function getContentManager()
    {
        return $this->get('vswsystem.orm.content.manager');
    }

}

