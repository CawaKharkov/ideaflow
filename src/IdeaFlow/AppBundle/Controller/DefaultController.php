<?php

namespace IdeaFlow\AppBundle\Controller;

use IdeaFlow\AppBundle\Controller\AbstractController as AppAbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/")
 */
class DefaultController extends AppAbstractController
{
    /**
     * @Route("/",name="homepage")
     */
    public function indexAction(Request $request)
    {
        $pageNumber = $request->query->get('page', 1);

        $ideas = $this->get('ideaflow.orm.service.idea')->getRepository()->getLastIdeas(6);

        $paginated = $this->get('ideaflow.orm.service.idea')->getPagenated($ideas, $pageNumber);

        $this->get('session')->getFlashBag()->add(
            'success', 'Test alert!');

        $searchFrom = $this->createForm('idea_search_type', null, ['action' => $this->generateUrl('ideas_search')]);

        $ideasCount = $this->get('ideaflow.orm.service.idea')->getCount();
        $usersCount = $this->get('ideaflow.orm.service.user')->getCount();

        $users = $this->get('ideaflow.orm.service.user')->findAll();

        return $this->render('IdeaFlowAppBundle:Default:index.html.twig',['ideas' => $paginated, 'searchForm' => $searchFrom->createView()
            , 'ideasCount' => $ideasCount, 'usersCount' => $usersCount, 'users' => $users]);
    }
}

