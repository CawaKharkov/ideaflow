<?php

namespace IdeaFlow\AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use IdeaFlow\AppBundle\Controller\AbstractController;

/**
 * @Route("/help")
 */
class HelpUserController extends AbstractController
{
    /**
     * @Route("/features/",name="how_it_works")
     * @Template()
     */
    public function indexAction()
    {
        return [];
    }
}

