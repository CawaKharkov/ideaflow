<?php

namespace IdeaFlow\AppBundle\Controller;

use IdeaFlow\AppBundle\Controller\AbstractController as AppAbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/page")
 */
class PageController extends AppAbstractController
{

    /**
     * @Route("/{alias}",name="view_page")
     * @Template()
     */
    public function viewAction(Request $request, $alias)
    {
        $page = $this->getPagesManager()->getPageByAlias($alias);
        $type = $this->getPagesManager()->getTypeByAlias($alias);

        $pageNumber = $request->query->get('page', 1);
        $pagination = null;
        switch ($type) {
            case 'TextContentPage':
                $view = 'VswSystemCmsBundle:Page:view_text.html.twig';
                break;

            case 'GalleryContentPage':
                $view = 'VswSystemCmsBundle:Page:view_gallery.html.twig';
                break;

            case 'FaqContentPage':
                $view = 'VswSystemCmsBundle:Page:view_faq.html.twig';
                $pagination = $this->get('vswsystem.pagination.faq')->getPagination($page->getQuestions(), $pageNumber);
                break;

            case 'NewsContentPage':
                $view = 'VswSystemCmsBundle:Page:view_news.html.twig';
                if ($page->getPaginatable() !== null && count($page->getEvents())>10) {
                    $pagination = $this->get('vswsystem.pagination.faq')->getPagination($page->getEvents(), $pageNumber);
                }

        }

        $canCreate = $this->get('security.context')->isGranted('ROLE_SUPER_ADMIN');
        if (!$page && $canCreate) {
            return $this->redirect($this->generateUrl('admin_pages'));
        }
        if (!$page) {
            throw new NotFoundHttpException("Page not found");
        }
        return $this->render($view, ['page' => $page, 'pagination' => $pagination]);

    }

    /**
     * @Route("/content/{alias}" , name="get_page_content")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function getContentAction(Request $request, $alias)
    {
        $page = $this->getPagesManager()->getPageByAlias($alias);

        $response = new \Symfony\Component\HttpFoundation\JsonResponse();

        if ($request->getMethod() == 'POST') {
            $newContent = $request->request->get('value');
            $page->setContent($newContent);

            $this->get('vswsystem.orm.content.manager')->save($page, true);
            $response->setData($this->container->get('markdown.parser')->transformMarkdown($newContent));
        } else {
            $response->setData($page->getContent());
        }

        return $response;
    }

}

