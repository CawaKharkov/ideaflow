<?php
/**
 * Locale listener
 *
 * User: cawa
 * Date: 8/22/14
 * Time: 6:26 PM
 */

namespace IdeaFlow\AppBundle\Locale\Listener;


use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class LocaleListener
 * @package IdeaFlow\AppBundle\Locale\Listener
 */
class LocaleListener  implements EventSubscriberInterface
{

    public function __construct($defaultLocale = 'lt_Lt')
    {
        $this->defaultLocale = $defaultLocale;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        $request->setLocale('ru');
        return;
        /*
        if (!$request->hasPreviousSession()) {
            return;
        }

        // try to see if the locale has been set as a _locale routing parameter
        if ($locale = $request->getLanguages()[0]) { //$locale = $request->attributes->get('_locale') ||
            $request->setLocale($locale);
        }
        if($request->getSession()->get('Locale')){
            $request->setLocale($request->getSession()->get('Locale'));
        }*/
    }

    public static function getSubscribedEvents()
    {
        return array(
            // must be registered before the default Locale listener
            KernelEvents::REQUEST => array(array('onKernelRequest', 17)),
        );
    }

} 
