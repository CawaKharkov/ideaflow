<?php

namespace IdeaFlow\AppBundle\Entity\Traits;

use Symfony\Component\Validator\Constraints as Assert;


/**
 * Class NamedEntity
 * @package IdeaFlow\AppBundle\Entity\Traits
 */
trait NamedEntity
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50)
     * @Assert\NotBlank
     * @Assert\Length(
     *      min = 3,
     *      max = 50,
     *      minMessage = "Name must be at least {{ limit }} characters long",
     *      maxMessage = "Name cannot be longer than {{ limit }} characters long"
     * )
     */
    protected $name;


    /**
     * Set name
     *
     * @param string $name
     * @return File
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
