<?php

namespace IdeaFlow\AppBundle\Entity\Traits;

/**
 * Class IsActiveEntity
 * @package IdeaFlow\AppBundle\Entity\Traits
 */
trait IsActiveEntity
{
    /**
     * @var boolean
     * @ORM\Column(name="is_active", type="boolean")
     */
    protected $isActive = 0;

    /**
     * @param mixed $isActive
     */
    public function setActive($isActive)
    {
        $this->isActive = $isActive;
    }



    /**
     * @return mixed
     */
    public function isActive()
    {
        return $this->isActive;
    }


}
