<?php

namespace IdeaFlow\AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use IdeaFlow\AppBundle\Entity\Traits\FindAll;

/**
 * Class AbstractRepository
 * Base class for all repos
 * @package IdeaFlow\AppBundle\Entity\Repository
 */
abstract class AbstractRepository extends EntityRepository
{

    use FindAll;

}

