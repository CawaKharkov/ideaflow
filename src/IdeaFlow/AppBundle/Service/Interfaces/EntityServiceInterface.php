<?php

namespace IdeaFlow\AppBundle\Service\Interfaces;

/**
 * Interface EntityServiceInterface
 * @package IdeaFlow\AppBundle\Service\Interfaces
 * All service that manipulates just entity must implements this interface
 */
interface EntityServiceInterface
{
    /**
     * Creates the entity
     * @return Object
     */
    public function create();

    /**
     * Get entity by id
     * @param $id
     * @return Object
     */
    public function get($id);

    /**
     * Find entities by criterias
     * @param $field
     * @param $value
     * @return Object[]
     */
    public function find($field,$value); // @TODO do i need it?

    /**
     * Remove entity
     * @param Entity $entity
     * @param bool $flush
     */
    public function remove($entity, $flush);

    /**
     * Save entity in databse
     * @param \Doctrine\ORM\Mapping\Entity $entity
     * @param bool $flush
     * @return Entity
     */
    public function save($entity, $flush = false);

    /**
     * Get entity repository
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    public function getRepository();


    /**
     * @return \Doctrine\ORM\EntityManager
     */
    public function getManager();

    /**
     * Get entity count
     * @return int
     */
    public function getCount();
}
