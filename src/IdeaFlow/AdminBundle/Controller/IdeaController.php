<?php

namespace IdeaFlow\AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use IdeaFlow\AdminBundle\Controller\AbstractController as AdminAbstractController;

/**
 * @Route("/admin/idea")
 */
class IdeaController extends AdminAbstractController
{
    /**
     * @Route("/",name="admin_idea")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        //var_dump($request->getLocale());
        $this->get('session')->getFlashBag()->add(
            'warning', 'Test admin alert!');
        return [];
    }
}

