<?php

namespace zaSkeleton;

/**
 * Main class for zaSkeleton
 *
 * @author cawa
 */
class Application
{
    protected $serviceContainer;
    
    protected $request;
    protected $response;

    protected $template;

    protected $layout;
    protected $view;
    
    protected $router;
    
    protected $config;

    protected $events;

    
}

